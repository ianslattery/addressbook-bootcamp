package app.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.hibernate.Session;

import hibernate.HibernateUtilities;
import main.AddressBookEntry;

public class AddressBookEntryDAOImpl implements AddressBookEntryDAO {

	private List<AddressBookEntry> abeList = new ArrayList<>();


	@SuppressWarnings("unchecked")
	public String createAddressBookEntry(AddressBookEntry abe) {

		Session session = HibernateUtilities.getSessionFactory().openSession();
		session.beginTransaction();
		abeList = session.createCriteria(AddressBookEntry.class).list();
		boolean uniqueEmail = true;
		for (AddressBookEntry eachEntry : abeList) {
			if (eachEntry.getEmailAddress().equals(abe.getEmailAddress())) {
				uniqueEmail = false;
				return "\nEmail already exists, create an entry with a unique email.\n";
			}
		}
		if (uniqueEmail == true) {
			session.save(abe);
			session.getTransaction().commit();
		}
		session.close();
		System.out.println("\n" + abe.formatToString());
		return "\nUser successfully created!";
	}

	@Override
	public AddressBookEntry getAddressBookEntry(String email) {
		try{
			Session session = HibernateUtilities.getSessionFactory().openSession();
			session.beginTransaction();
			AddressBookEntry abe = session.get(AddressBookEntry.class, email);
			session.close();
			return abe;
		} catch(EntityNotFoundException e) {
			e.printStackTrace();
		}
		return null;

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<AddressBookEntry> getAllAddressBookEntries() {
		
		try{
		Session session = HibernateUtilities.getSessionFactory().openSession();
		session.beginTransaction();
		abeList = session.createCriteria(AddressBookEntry.class).list();
		session.close();
		return abeList;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String updateAddressBookEntry(AddressBookEntry changedAbe) {

		try{
		Session session = HibernateUtilities.getSessionFactory().openSession();
		session.beginTransaction();

		session.update(changedAbe);
		session.getTransaction().commit();
		session.close();
		} catch(Exception e) {
			e.printStackTrace();
		}

		return "complete";
	}

	@Override
	public void deleteAddressBookEntry(AddressBookEntry deletionAbe) {

		Session session = HibernateUtilities.getSessionFactory().openSession();
		session.beginTransaction();

		session.delete(deletionAbe);
		session.getTransaction().commit();
		session.close();
		
	}
}
