package main;

public class AddressBookEntry {
	private String name;
	private String address;
	private String emailAddress;
	private String phoneNumber;
	private String zipcode;
	
	public AddressBookEntry(String name, String address, String emailAddress, String phoneNumber,  String zipcode){
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.emailAddress = emailAddress;
		this.zipcode = zipcode;
	}
	
	public AddressBookEntry(AddressBookEntry abe) {
		this.name = abe.getName();
		this.address = abe.getAddress();
		this.phoneNumber = abe.getPhoneNumber();
		this.emailAddress = abe.getEmailAddress();
		this.zipcode = abe.getZipcode();
	}
	
	public AddressBookEntry(){
		this.name = "invalid_ABE";
		this.address = "invalid_ABE";
		this.phoneNumber = "invalid_ABE";
		this.emailAddress = "invalid_ABE";
		this.zipcode = "invalid_ABE";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String toString(){
		return  name + ", " + address + ", " + emailAddress + ", " + phoneNumber + ", " + zipcode;
	}
	
	public String formatToString(){
		return  "________________________________" +
				"\nName: " + name + ", " +
				"\nAddress: " + address + ", " +
				"\nEmail: " + emailAddress + ", " +
				"\nPhone: " + phoneNumber + ", " +
				"\nZipcode: " + zipcode + 
				"\n________________________________\n";
	}
}
