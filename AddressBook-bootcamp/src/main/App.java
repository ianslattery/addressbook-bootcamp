package main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

import app.dao.AddressBookEntryDAOImpl;
import comparator.NameSortComparator;
import comparator.ZipSortComparator;
import hibernate.HibernateUtilities;

public class App {

	public static void main(String[] args) {
		AddressBookEntryDAOImpl abeDAO = new AddressBookEntryDAOImpl();
		List<AddressBookEntry> abeList = abeDAO.getAllAddressBookEntries();
		NameSortComparator nameSort = new NameSortComparator();
		ZipSortComparator zipSort = new ZipSortComparator();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int choice = 0;
		String email = "";
		String menuOptions = "\n\n**************MENU*******************"
				+ "\nPlease choose an option: \n\n" 
				+ "1: See all addresses \n" 
				+ "2: Add new comtact \n"
				+ "3: Delete Contact \n" 
				+ "4: Edit Contact \n" 
				+ "5: Save changes and exit \n"
				+ "6: Sort\n" 
				+ "7: Find specific user" 
				+ "\n\n*************************************";
		
		while (choice != 5) {
			System.out.println(menuOptions);

			try {
				choice = Integer.parseInt(br.readLine());
			} catch (Exception e) {
				choice = 0;
			}
			switch (choice) {
			case 0:
				System.out.println("invalid input");
				break;

			case 1:
				abeList = abeDAO.getAllAddressBookEntries();
				for (AddressBookEntry address : abeList) {
					System.out.println(address.formatToString());
				}
				break;

			case 2:
				AddressBookEntry abe = new AddressBookEntry();

				try {
					System.out.println("enter first name");String fname = br.readLine();
					System.out.println("enter last name");String lname = br.readLine();
					abe.setName((fname + " " + lname).toString());
					
					System.out.println("enter address"); abe.setAddress(br.readLine());
					System.out.println("enter email"); abe.setEmailAddress(br.readLine());
					System.out.println("enter phone number"); abe.setPhoneNumber(br.readLine());
					System.out.println("enter zip code"); abe.setZipcode(br.readLine());
					
				} catch (Exception e) {
					choice = 0;
				}

				System.out.println(abeDAO.createAddressBookEntry(abe));
				break;

			case 3:
				System.out.println("enter the email address of entry to delete");
				try {
					email = br.readLine();
				} catch (Exception e) {
					choice = 0;
				}
				AddressBookEntry entryForDeletion = abeDAO.getAddressBookEntry(email);
				if (entryForDeletion == null) {
					System.out.println("");
					System.out.println("entry not found");
				} else {
					System.out.println("");
					System.out.println(entryForDeletion.formatToString());
					System.out.println("");
					System.out.println("\n*************************************"
									+"\nIs this the record you wish to delete? " 
									+ "\n1: yes  \n" 
									+ "\n2: no"
									+ "\n*************************************");
					System.out.println("");

					try {
						int answer = Integer.parseInt(br.readLine());

						if (answer == 1) {
							abeDAO.deleteAddressBookEntry(entryForDeletion);
							System.out.println("record deleted");
							System.out.println("");
						}
					} catch (Exception e) {
						choice = 0;
					}
				}

				break;

			case 4:
				System.out.println("enter the email address of entry to edit");
				try {
					email = br.readLine();
				} catch (Exception e) {
					choice = 0;
				}
				AddressBookEntry entryForEdit = abeDAO.getAddressBookEntry(email);

				if (entryForEdit == null) {
					System.out.println("");
					System.out.println("entry not found matching that email");
				} else {
					System.out.println("");
					System.out.println(entryForEdit.formatToString());
					System.out.println("");
					System.out.println("\n*************************************"
							+"\nIs this the record you wish to edit? " 
							+ "\n1: yes  \n" 
							+ "\n2: no"
							+ "\n*************************************");
					System.out.println("");

					int answer = 2;
					try {
						answer = Integer.parseInt(br.readLine());
					} catch (Exception e) {
						choice = 0;
					}
					if (answer == 1) {
						System.out.println("\n*************************************"
								+ "\nPlease choose an option: \n\n" 
								+ "1: change address \n"
								+ "2: change name \n" 
								+ "3: change phone number \n" 
								+ "4: change zip code"
								+ "\n*************************************");

						System.out.println("");
						int editOptions = 0;
						try {
							editOptions = Integer.parseInt(br.readLine());
						} catch (Exception e) {
							choice = 0;
						}
						switch (editOptions) {

						case 1:
							System.out.println("enter a new address");
							try {
								entryForEdit.setAddress(br.readLine());
							} catch (Exception e) {
								choice = 0;
							}
							System.out.println("");
							System.out.println("New Details: ");
							System.out.println("");
							System.out.println(entryForEdit);
							break;

						case 2:
							
							try {
								System.out.println("enter a new first name");
								String fname = br.readLine();
								System.out.println("enter a new last name");
								String lname = br.readLine();
								entryForEdit.setName(fname + " " + lname);
							} catch (Exception e) {
								choice = 0;
							}
							System.out.println("");
							System.out.println("New Details: ");
							System.out.println("");
							System.out.println(entryForEdit.formatToString());
							break;

						case 3:
							System.out.println("enter a new phone number");
							try {
								entryForEdit.setPhoneNumber(br.readLine());
							} catch (Exception e) {
								choice = 0;
							}
							System.out.println("");
							System.out.println("New Details: ");
							System.out.println("");
							System.out.println(entryForEdit);
							break;

						case 4:
							System.out.println("enter a new zip code");
							try {
								entryForEdit.setZipcode(br.readLine());
							} catch (Exception e) {
								choice = 0;
							}
							System.out.println("");
							System.out.println("New Details: ");
							System.out.println("");
							System.out.println(entryForEdit);
							break;

						default:
							System.out.println("invalid selection");
							break;
						}
						abeDAO.updateAddressBookEntry(entryForEdit);
					}
				}
				break;

			case 5:
				System.out.println("changes saved");
				HibernateUtilities.getSessionFactory().close();
				break;

			case 6:
				System.out.println("\n*************************************"
									+"\nSelect "
									+ "\n'n' for name sort"
									+ "\n'z' for zipcode sort."
									+"\n*************************************");
				String sort ="n";
				try{
					sort = br.readLine().toLowerCase();
				} catch (Exception e) {
					choice = 0;
				}
				abeList = abeDAO.getAllAddressBookEntries();
				if(sort.equals("n")){
					Collections.sort(abeList, nameSort.abeNameSort);
				} else if(sort.equals("z")){
					Collections.sort(abeList, zipSort.abeZipSort);
				}
				for (AddressBookEntry address : abeList) {
					System.out.println(address.formatToString());
				}
				break;
				
			case 7: 
				System.out.println("enter the email address of entry");
				try {
					email = br.readLine();
				} catch (Exception e) {
					choice = 0;
				}
				AddressBookEntry entryLookup = abeDAO.getAddressBookEntry(email);
				if(entryLookup != null){
					System.out.println(entryLookup.formatToString());
				} else {
					System.out.println("\nemail address doesn't exist");
				}
				break;

			default:
				System.out.println("\ninvalid selection");
				break;

			}
		}
	}
	
}
