package comparator;

import java.util.Comparator;

import main.AddressBookEntry;

public class ZipSortComparator {
	public Comparator<AddressBookEntry> abeZipSort = new Comparator<AddressBookEntry>(){	
		@Override
		public int compare(AddressBookEntry abe1, AddressBookEntry abe2){
			String abe1Zip = abe1.getZipcode().toLowerCase();
			String abe2Zip = abe2.getZipcode().toLowerCase();
			
			return abe1Zip.compareTo(abe2Zip);
		}
	};
}
