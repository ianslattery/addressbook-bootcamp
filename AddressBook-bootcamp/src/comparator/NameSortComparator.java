package comparator;

import java.util.Comparator;

import main.AddressBookEntry;

public class NameSortComparator {
	public Comparator<AddressBookEntry> abeNameSort = new Comparator<AddressBookEntry>(){		
		@Override
		public int compare(AddressBookEntry abe1, AddressBookEntry abe2){
			String abe1Name = abe1.getName().toLowerCase();
			String abe2Name = abe2.getName().toLowerCase();
			
			return abe1Name.compareTo(abe2Name);
		}
	};
}
